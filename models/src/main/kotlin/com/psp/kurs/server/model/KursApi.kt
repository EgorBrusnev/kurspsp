package com.psp.kurs.server.model

import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime

open class KursApi {

    open class Object : Serializable

    open class Function : Serializable {
        open fun execute(): Object {
            return Object()
        }
    }

    open class SendLoginRequest(val username: String?, val password: String?) : Function()

    open class SendLoginToken(val token: String?) : Function()

    class AuthenticationResult() : Object() {
        var token: String? = null
        var isSuccess: Boolean = false
        var role: String? = null

        constructor(token: String? = null, isSuccess: Boolean, role: String? = null) : this() {
            this.isSuccess = isSuccess
            this.token = token
            this.role = role
        }
    }

    open class Error(val error: String) : Object()

    open class Ok : Object()

    open class Account(val username: String, val password: String, val role: String) : Object()

    open class User(
        val id: Int = -1,
        val username: String,
        val surname: String,
        val name: String,
        val patronymic: String,
        val age: Int,
        val groupNumber: Int,
        val education: String,
        val englishLevel: String,
        val role: String
    ) : Object()

    open class UserList(val users: List<User>) : Object()

    open class Schedule(
        val id: Int = -1,
        val lesson: String,
        val groupId: Int,
        val teacher: String,
        val room: String,
        val startAt: LocalDateTime,
        val endAt: LocalDateTime
    ) : Object()

    open class Mark(
        val id: Int = -1,
        val username: String,
        val value: Int,
        val date: LocalDate
    ) : Object()

    open class Group(
        val id: Int = -1,
        val lang: String,
        val students: List<User> = emptyList()
    ) : Object()

    open class GroupList(val groups: List<Group>) : Object()

    open class MarkList(val marks: List<Mark>) : Object()

    open class ScheduleList(val lessons: List<Schedule>) : Object()

    open class RegisterUser(val account: Account, val user: User) : Function()

    open class GetUserByToken(val token: String) : Function()

    open class GetScheduleByGroup(val groupId: Int) : Function()

    open class GetMarksByUsername(val username: String) : Function()

    open class GetUnRegisteredUsers : Function()

    open class SetUserGroup(val username: String, val groupId: Int) : Function()

    open class AddNewSchedule(val schedule: Schedule) : Function()

    open class GetMarksForGroup(val groupId: Int) : Function()

    open class GetUsersForGroup(val groupId: Int) : Function()

    open class AddMark(val mark: KursApi.Mark) : Function()

    open class GetUsersByRole(val role: String) : Function()

    open class UpdateUsers(val users: UserList) : Function()

    open class DeleteUser(val id: Int) : Function()

    open class GetAllUsers() : Function()

    open class GetAllGroups() : Function()
}