package com.psp.kurs.server.utils

enum class UserRoles {
    ADMIN, USER, TEACHER
}

enum class Education {
    HIGH, MIDDLE, BASE
}

enum class EnglishLevel {
    UPPER_INTERMEDIATE, INTERMEDIATE, PRE_INTERMEDIATE
}