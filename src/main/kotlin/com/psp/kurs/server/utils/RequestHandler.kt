package com.psp.kurs.server.utils

import com.psp.kurs.server.api.admin.GetUnregisteredUsersImpl
import com.psp.kurs.server.api.admin.GetUsersByRoleImpl
import com.psp.kurs.server.api.admin.SetUserGroupImpl
import com.psp.kurs.server.api.groups.GetAllGroupsImpl
import com.psp.kurs.server.api.lessons.*
import com.psp.kurs.server.api.login.GetUserByTokenImpl
import com.psp.kurs.server.api.login.RegisterUserImpl
import com.psp.kurs.server.api.login.SendLoginRequestImpl
import com.psp.kurs.server.api.login.SendLoginTokenImpl
import com.psp.kurs.server.api.users.DeleteUserImpl
import com.psp.kurs.server.api.users.GetAllUsersImpl
import com.psp.kurs.server.api.users.GetUsersForGroupImpl
import com.psp.kurs.server.api.users.UpdateUsersImpl
import com.psp.kurs.server.model.KursApi
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.lang.Exception
import java.net.Socket

class RequestHandler(private val socket: Socket) : Thread() {

    private val router: Router = Router()

    override fun run() {
        val oos = ObjectOutputStream(socket.getOutputStream())
        val ois = ObjectInputStream(socket.getInputStream())
        try {
            println("Received connection")
            while (true) {
                (ois.readObject() as KursApi.Function?)?.let {
                    val response = router.executeFunction(it)
                    println("sending response to client: $response")
                    oos.writeObject(response)
                    oos.flush()
                } ?: break
            }
        } catch (e: Exception) {
            println("Error: ${e.localizedMessage}")
            e.printStackTrace()
        } finally {
            oos.close()
            ois.close()
            socket.close()
            println("connection closed")
        }
    }
}

class Router {
    fun executeFunction(func: KursApi.Function) = func.let {
        when (it) {
            is KursApi.SendLoginRequest -> SendLoginRequestImpl(it.username, it.password).execute()
            is KursApi.SendLoginToken -> SendLoginTokenImpl(it.token).execute()
            is KursApi.RegisterUser -> RegisterUserImpl(it.account, it.user).execute()
            is KursApi.GetUserByToken -> GetUserByTokenImpl(it.token).execute()
            is KursApi.GetScheduleByGroup -> GetScheduleByGroupImpl(it.groupId).execute()
            is KursApi.GetMarksByUsername -> GetMarksByUsernameImpl(it.username).execute()
            is KursApi.GetUnRegisteredUsers -> GetUnregisteredUsersImpl().execute()
            is KursApi.SetUserGroup -> SetUserGroupImpl(it.username, it.groupId).execute()
            is KursApi.AddNewSchedule -> AddNewScheduleImpl(it.schedule).execute()
            is KursApi.GetMarksForGroup -> GetMarksForGroupImpl(it.groupId).execute()
            is KursApi.GetUsersForGroup -> GetUsersForGroupImpl(it.groupId).execute()
            is KursApi.AddMark -> AddMarkImpl(it.mark).execute()
            is KursApi.GetUsersByRole -> GetUsersByRoleImpl(it.role).execute()
            is KursApi.UpdateUsers -> UpdateUsersImpl(it.users).execute()
            is KursApi.DeleteUser -> DeleteUserImpl(it.id).execute()
            is KursApi.GetAllUsers -> GetAllUsersImpl().execute()
            is KursApi.GetAllGroups -> GetAllGroupsImpl().execute()
            else -> println("Unsupported function")
        }
    }
}