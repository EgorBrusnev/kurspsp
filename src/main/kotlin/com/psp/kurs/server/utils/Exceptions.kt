package com.psp.kurs.server.utils

class RegistrationException(override val message: String) : Exception(message)