package com.psp.kurs.server.utils

import com.psp.kurs.server.entity.*
import org.hibernate.SessionFactory
import org.hibernate.cfg.Configuration
import org.hibernate.boot.registry.StandardServiceRegistryBuilder

object HibernateSessionFactory {
    private var sessionFactory: SessionFactory? = null

    fun getSessionFactory(): SessionFactory = sessionFactory ?: Configuration().configure()
        .run {
            addAnnotatedClass(AccountAo::class.java)
            addAnnotatedClass(UserAo::class.java)
            addAnnotatedClass(ScheduleAo::class.java)
            addAnnotatedClass(MarkAo::class.java)
            addAnnotatedClass(GroupAo::class.java)
            buildSessionFactory(
                StandardServiceRegistryBuilder()
                    .applySettings(properties)
                    .build()
            ).also { sessionFactory = it }
        }

}