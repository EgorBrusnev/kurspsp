package com.psp.kurs.server.dao

import com.psp.kurs.server.entity.GroupAo
import com.psp.kurs.server.entity.UserAo
import com.psp.kurs.server.utils.HibernateSessionFactory
import org.hibernate.Session
import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.criterion.Restrictions
import org.hibernate.sql.JoinType

class GroupDao {

    private val session: Session = HibernateSessionFactory
        .getSessionFactory()
        .openSession()

    fun getAll() = (session.createCriteria(GroupAo::class.java, "group")
        .createAlias("group.users", "user")
        .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
        .list() as List<GroupAo>)
        .also { session.clear() }
}

fun main(args: Array<String>) {
    val a = GroupDao().getAll()
    println(a)
}