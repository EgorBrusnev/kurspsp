package com.psp.kurs.server.dao

import com.psp.kurs.server.entity.AccountAo
import com.psp.kurs.server.entity.MarkAo
import com.psp.kurs.server.utils.HibernateSessionFactory
import com.psp.kurs.server.utils.UserRoles
import org.hibernate.Session
import org.hibernate.Transaction
import org.hibernate.criterion.Restrictions
import org.hibernate.sql.JoinType
import java.lang.Exception

class MarkDao {

    private val session: Session = HibernateSessionFactory
        .getSessionFactory()
        .openSession()

    fun getMarksForUser(username: String) =
        (session.createCriteria(MarkAo::class.java)
            .createCriteria("user", "user", JoinType.INNER_JOIN).add(Restrictions.eq("user.account.username", username))
            .list() as List<MarkAo>)
            .also { session.clear() }

    fun save(mark: MarkAo): MarkAo? {
        var transaction: Transaction? = null
        return try {
            transaction = session.transaction.apply {
                begin()
                session.save(mark)
                commit()
            }
            session.clear()
            mark
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }

    fun deleteMark(id: Int): MarkAo? {
        var transaction: Transaction? = null
        var mark: MarkAo? = null
        return try {
            transaction = session.transaction.apply {
                begin()
                mark = session.get(MarkAo::class.java, id)
                session.delete(mark)
                commit()
            }
            session.clear()
            mark
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }

    fun getMarksForGroup(group: Int) =
        (session.createCriteria(MarkAo::class.java)
            .createCriteria("user", "user", JoinType.INNER_JOIN).add(Restrictions.eq("user.groupNumber", group))
            .list() as List<MarkAo>)
            .also { session.clear() }
}