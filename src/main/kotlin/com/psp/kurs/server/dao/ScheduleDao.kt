package com.psp.kurs.server.dao

import com.psp.kurs.server.entity.AccountAo
import com.psp.kurs.server.entity.ScheduleAo
import com.psp.kurs.server.utils.HibernateSessionFactory
import org.hibernate.Session
import org.hibernate.Transaction
import org.hibernate.criterion.Restrictions
import java.lang.Exception

class ScheduleDao {

    private val session: Session = HibernateSessionFactory
        .getSessionFactory()
        .openSession()

    fun save(schedule: ScheduleAo): ScheduleAo? {
        var transaction: Transaction? = null
        return try {
            transaction = session.transaction.apply {
                begin()
                session.saveOrUpdate(schedule)
                commit()
            }
            session.clear()
            schedule
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }

    fun findScheduleByGroupId(groupId: Int) =
        (session.createCriteria(ScheduleAo::class.java)
            .add(Restrictions.eq("groupId", groupId))
            .list() as List<ScheduleAo>)
            .also { session.clear() }
}