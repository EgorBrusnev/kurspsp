package com.psp.kurs.server.dao

import com.psp.kurs.server.entity.AccountAo
import com.psp.kurs.server.entity.UserAo
import com.psp.kurs.server.utils.HibernateSessionFactory
import org.hibernate.Session
import org.hibernate.Transaction
import org.hibernate.criterion.Restrictions
import java.lang.Exception

class AccountsDao {

    private val session: Session = HibernateSessionFactory
        .getSessionFactory()
        .openSession()

    fun findByUsername(username: String?) =
        (session.createCriteria(AccountAo::class.java)
            .add(Restrictions.eq("username", username))
            .list().firstOrNull() as AccountAo?)
            .also { session.clear() }

    fun findByToken(token: String?) =
        (session.createCriteria(AccountAo::class.java)
            .add(Restrictions.eq("token", token))
            .list().firstOrNull() as AccountAo?)
            .also { session.clear() }

    fun updateAccount(entity: AccountAo): AccountAo? {
        var transaction: Transaction? = null
        return try {
            transaction = session.transaction.apply {
                begin()
                session.saveOrUpdate(entity)
                commit()
            }
            session.clear()
            entity
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }

    fun createAccount(entity: AccountAo): AccountAo? {
        var transaction: Transaction? = null
        return try {
            transaction = session.transaction.apply {
                begin()
                session.save(entity)
                commit()
            }
            session.clear()
            entity
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }
}