package com.psp.kurs.server.dao

import com.psp.kurs.server.entity.AccountAo
import com.psp.kurs.server.entity.MarkAo
import com.psp.kurs.server.entity.UserAo
import com.psp.kurs.server.utils.HibernateSessionFactory
import com.psp.kurs.server.utils.UserRoles
import org.hibernate.Criteria
import org.hibernate.Session
import org.hibernate.Transaction
import org.hibernate.criterion.Projection
import org.hibernate.criterion.Projections
import org.hibernate.criterion.Restrictions
import org.hibernate.sql.JoinType
import tornadofx.Rest
import java.lang.Exception

class UserDao {

    private val session: Session = HibernateSessionFactory
        .getSessionFactory()
        .openSession()

    fun save(userAo: UserAo): UserAo? {
        var transaction: Transaction? = null
        return try {
            transaction = session.transaction.apply {
                begin()
//                session.saveOrUpdate(userAo)
                findByUsername(userAo.account.username)?.let {
                    session.update(userAo)
                } ?: session.save(userAo)
                commit()
            }
            session.clear()
            userAo
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }

    fun findByRole(role: UserRoles) = (session.createCriteria(UserAo::class.java)
        .createCriteria("account", "acc", JoinType.INNER_JOIN).add(Restrictions.eq("acc.role", role))
        .list() as List<UserAo>)
        .also { session.clear() }

    fun findByUsername(username: String) =
        (session.createCriteria(UserAo::class.java)
            .add(Restrictions.eq("account.username", username))
            .list().firstOrNull() as UserAo?)
            .also { session.clear() }


    fun findByGroup(group: Int) = (session.createCriteria(UserAo::class.java)
        .add(Restrictions.eq("group", group))
        .createCriteria("account", "acc", JoinType.INNER_JOIN).add(Restrictions.eq("acc.role", UserRoles.USER))
        .list() as List<UserAo>)
        .also { session.clear() }

    fun getUnregisteredUsers() = findByGroup(-1)

    fun deleteUser(id: Int): UserAo? {
        var transaction: Transaction? = null
        var user: UserAo? = null
        return try {
            transaction = session.transaction.apply {
                begin()
                user = session.get(UserAo::class.java, id)
                session.delete(user)
                commit()
            }
            session.clear()
            user
        } catch (e: Exception) {
            transaction?.rollback()
            e.printStackTrace()
            null
        }
    }

    fun getAllUsers() = (session.createCriteria(UserAo::class.java)
        .list() as List<UserAo>)
        .also { session.clear() }
}