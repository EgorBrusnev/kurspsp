package com.psp.kurs.server.dto

import java.time.LocalDateTime

data class ScheduleDto(
        val lesson: String,
        val group: Int,
        val teacher: String,
        val room: String,
        val startAt: LocalDateTime,
        val endAt: LocalDateTime
)