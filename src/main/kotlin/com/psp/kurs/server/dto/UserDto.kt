package com.psp.kurs.server.dto

import com.psp.kurs.server.utils.Education
import com.psp.kurs.server.utils.EnglishLevel

data class UserDto(
    val username: String,
    val surname: String,
    val name: String,
    val patronymic: String,
    val age: Int,
    val education: Education,
    val englishLevel: EnglishLevel
)