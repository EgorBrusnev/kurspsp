package com.psp.kurs.server.dto

import com.psp.kurs.server.entity.AccountAo
import com.psp.kurs.server.utils.UserRoles

data class AccountDto(
    val username: String,
    val password: String,
    val token: String? = null,
    val role: UserRoles
) {
    fun toAo() = AccountAo(username = username, password = password, token = token, role = role)
}