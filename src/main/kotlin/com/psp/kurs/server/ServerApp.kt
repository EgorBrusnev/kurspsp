package com.psp.kurs.server

import com.psp.kurs.server.dao.AccountsDao
import com.psp.kurs.server.utils.HibernateSessionFactory
import com.psp.kurs.server.utils.RequestHandler
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.net.ServerSocket
import java.net.Socket
import javax.persistence.Persistence

class ServerApp(private val port: Int) : Thread() {
    private lateinit var serverSocket: ServerSocket
    private var running: Boolean = false

    fun startServer() {
        try {
            HibernateSessionFactory
                .getSessionFactory()
                .openSession()
            serverSocket = ServerSocket(port)
            start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun stopServer() {
        running = false
        interrupt()
    }

    override fun run() {
        running = true
        while (running) {
            try {
                println("Listenning connection")
                val socket: Socket = serverSocket.accept()
                val requestHandler = RequestHandler(socket)
                requestHandler.start()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}

fun main(args: Array<String>) {

    val server = ServerApp(8000)
    println("Server start on port: " + 8000)
    server.startServer()
    while (true) {
    }
    server.stopServer()
}