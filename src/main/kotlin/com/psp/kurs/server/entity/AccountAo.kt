package com.psp.kurs.server.entity

import com.psp.kurs.server.dto.AccountDto
import com.psp.kurs.server.utils.UserRoles
import java.io.Serializable
import java.util.Date
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "accounts")
data class AccountAo(
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(updatable = false, nullable = false)
    var id: Int = -1,
    @Column(name = "username", unique = true)
    var username: String = "",
    @Column(name = "password")
    var password: String = "",
    @Column(name = "token")
    var token: String? = null,
    @Enumerated(value = EnumType.STRING)
    @Column(name = "role", columnDefinition = "enum('ADMIN','USER','TEACHER')")
    var role: UserRoles = UserRoles.USER
) : Serializable

fun AccountAo.toDto() = AccountDto(username, password, token, role)