package com.psp.kurs.server.entity

import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "mark")
data class MarkAo(
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(updatable = false, nullable = false)
    var id: Int = -1,

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "username", referencedColumnName = "username", unique = true)
    var user: UserAo = UserAo(),

    @Column(name = "value")
    var value: Int = -1,

    @Column(name = "date")
    var date: LocalDate = LocalDate.now()
) : Serializable