package com.psp.kurs.server.entity

import java.io.Serializable
import java.sql.Timestamp
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "schedule")
data class ScheduleAo(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE)
    var id: Int = 0,

    @Column(name = "lesson")
    var lesson: String = "",

    @Column(name = "groupId")
    var groupId: Int = -1,

    @Column(name = "teacher")
    var teacher: String = "",

    @Column(name = "room")
    var room: String = "",

    @Column(name = "startAt")
    var startAt: Timestamp = Timestamp.valueOf(LocalDateTime.now()),

    @Column(name = "endAt")
    var endAt: Timestamp = Timestamp.valueOf(LocalDateTime.now()),

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    var group: GroupAo = GroupAo()

) : Serializable