package com.psp.kurs.server.entity

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import javax.persistence.*


@Entity
@Table(name = "`group`")
data class GroupAo(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int = -1,

    @Column(name = "lang")
    var language: String = "",
//
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "group", cascade = [CascadeType.ALL])
    @Fetch(value = FetchMode.SUBSELECT)
    var users: List<UserAo> = emptyList()
)