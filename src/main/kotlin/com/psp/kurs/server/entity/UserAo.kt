package com.psp.kurs.server.entity

import com.psp.kurs.server.utils.Education
import com.psp.kurs.server.utils.EnglishLevel
import com.psp.kurs.server.utils.UserRoles
import java.io.Serializable
import java.sql.Timestamp
import java.util.Date
import javax.persistence.*

@Entity
@Table(name = "user", uniqueConstraints = [UniqueConstraint(columnNames = ["username"])])
data class UserAo(
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE)
    var id: Int = 0,

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "username", referencedColumnName = "username", unique = true)
    var account: AccountAo = AccountAo(),

    @Column(name = "surname")
    var surname: String = "",

    @Column(name = "name")
    var name: String = "",

    @Column(name = "patronymic")
    var patronymic: String = "",

    @Column(name = "age")
    var age: Int = -1,

    @Column(name = "groupNumber")
    var groupNumber: Int = -1,

//    @ManyToOne(fetch=FetchType.LAZY)
    @Column(name = "group_id")
    var group: Int = -1,

    @Column(name = "education")
    @Enumerated(value = EnumType.STRING)
    var education: Education = Education.BASE,

    @Column(name = "english_level")
    @Enumerated(value = EnumType.STRING)
    var englishLevel: EnglishLevel = EnglishLevel.INTERMEDIATE
) : Serializable