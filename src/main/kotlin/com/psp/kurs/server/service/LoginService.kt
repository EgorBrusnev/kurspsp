package com.psp.kurs.server.service

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.psp.kurs.server.dao.AccountsDao
import com.psp.kurs.server.dao.UserDao
import com.psp.kurs.server.dto.AccountDto
import com.psp.kurs.server.dto.UserDto
import com.psp.kurs.server.entity.UserAo
import com.psp.kurs.server.entity.toDto
import com.psp.kurs.server.utils.RegistrationException
import javax.inject.Named

@Named
class LoginService {

    private val accountsDao: AccountsDao = AccountsDao()
    private val userDao: UserDao = UserDao()

    private fun generateToken(username: String, password: String): String = JWT.create()
        .withIssuer("auth0")
        .withClaim("username", username)
        .withClaim("password", password)
        .sign(Algorithm.HMAC256("secret"))

    fun authenticateUser(username: String?, password: String?) =
        accountsDao.findByUsername(username)?.let {
            if (password == it.password) {
                accountsDao.updateAccount(it.copy(token = generateToken(it.username, it.password)))
            } else null //TODO: make throw exception
        }?.toDto()

    private fun getUser(username: String) = accountsDao.findByUsername(username)

    fun authenticateUser(token: String?) = accountsDao.findByToken(token)?.toDto()

    fun registerUser(accountDto: AccountDto, userDto: UserDto): UserAo = getUser(accountDto.username)
        ?.let { throw RegistrationException("The user ${accountDto.username} already exists") }
        ?: let {
            accountsDao.createAccount(
                accountDto.toAo().copy(token = generateToken(accountDto.username, accountDto.password))
            )?.let {
                userDao.save(
                    UserAo(
                        account = it,
                        surname = userDto.surname,
                        name = userDto.name,
                        patronymic = userDto.patronymic,
                        age = userDto.age,
                        education = userDto.education,
                        englishLevel = userDto.englishLevel
                    )
                ) ?: throw RegistrationException("Cannot create user")
            } ?: throw RegistrationException("Cannot create account")
        }
}