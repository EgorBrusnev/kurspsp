package com.psp.kurs.server.service

import com.psp.kurs.server.dao.GroupDao
import com.psp.kurs.server.model.KursApi

class GroupService {
    private val groupDao = GroupDao()

    fun getAllGroups() = groupDao.getAll().takeIf { it.isNotEmpty() }?.let {
        KursApi.GroupList(it.map {
            KursApi.Group(
                it.id,
                it.language,
                it.users.map {
                    KursApi.User(
                        it.id,
                        it.account.username,
                        it.surname,
                        it.name,
                        it.patronymic,
                        it.age,
                        it.groupNumber,
                        it.education.name,
                        it.englishLevel.name,
                        it.account.role.name
                    )
                }
            )
        })
    } ?: KursApi.Error("No group found")
}