package com.psp.kurs.server.service

import com.psp.kurs.server.dao.MarkDao
import com.psp.kurs.server.dao.UserDao
import com.psp.kurs.server.entity.MarkAo
import com.psp.kurs.server.model.KursApi

class MarkService {

    private val markDao: MarkDao = MarkDao()
    private val userDao = UserDao()

    fun getMarksForUser(username: String) = markDao.getMarksForUser(username).sortedBy { it.date }

    fun getMarksForGroup(group: Int) = markDao.getMarksForGroup(group).sortedBy { it.date }

    fun addMark(mark: KursApi.Mark) = userDao.findByUsername(mark.username)?.let {
        markDao.save(MarkAo(user = it, value = mark.value, date = mark.date))
    }
}