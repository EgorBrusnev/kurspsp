package com.psp.kurs.server.service

import com.psp.kurs.server.dao.ScheduleDao
import com.psp.kurs.server.entity.ScheduleAo
import com.psp.kurs.server.model.KursApi
import java.sql.Timestamp

class ScheduleService {

    private val scheduleAo = ScheduleDao()

    fun getScheduleByGroup(groupId: Int) = scheduleAo.findScheduleByGroupId(groupId)

    fun addNewSchedule(schedule: KursApi.Schedule) =
        scheduleAo.save(
            ScheduleAo(
                lesson = schedule.lesson,
                groupId = schedule.groupId,
                teacher = schedule.teacher,
                room = schedule.room,
                startAt = Timestamp.valueOf(schedule.startAt),
                endAt = Timestamp.valueOf(schedule.endAt)
            )
        )


}