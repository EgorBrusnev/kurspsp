package com.psp.kurs.server.service

import com.psp.kurs.server.dao.AccountsDao
import com.psp.kurs.server.dao.UserDao
import com.psp.kurs.server.entity.UserAo
import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.utils.Education
import com.psp.kurs.server.utils.EnglishLevel
import com.psp.kurs.server.utils.UserRoles

class UserService {
    private val userDao = UserDao()
    private val accountsDao = AccountsDao()

    fun getUserByToken(token: String) = accountsDao.findByToken(token)
        ?.let { account ->
            userDao.findByUsername(account.username)
                ?.run {
                    KursApi.User(
                        id,
                        account.username,
                        surname,
                        name,
                        patronymic,
                        age,
                        groupNumber,
                        education.name,
                        englishLevel.name,
                        account.role.name
                    )
                }
                ?: KursApi.Error("Unable to find user")
        } ?: KursApi.Error("Unable to find account")

    fun getUnregisteredUsers(): KursApi.Object = userDao.getUnregisteredUsers().takeIf { it.isNotEmpty() }
        ?.run {
            KursApi.UserList(
                map { user ->
                    KursApi.User(
                        user.id,
                        user.account.username,
                        user.surname,
                        user.name,
                        user.patronymic,
                        user.age,
                        user.groupNumber,
                        user.education.name,
                        user.englishLevel.name,
                        user.account.role.name
                    )
                }
            )
        } ?: KursApi.Error("No requests")

    fun addUserToGroup(username: String, group: Int) = userDao.findByUsername(username)?.let {
        userDao.save(it.copy(groupNumber = group))
        KursApi.Ok()
    } ?: KursApi.Error("No such user")

    fun findUsersForGroup(group: Int) = userDao.findByGroup(group)

    fun findUsersByRole(role: UserRoles) = userDao.findByRole(role).takeIf { it.isNotEmpty() }
        ?.run {
            KursApi.UserList(
                map { user ->
                    KursApi.User(
                        user.id,
                        user.account.username,
                        user.surname,
                        user.name,
                        user.patronymic,
                        user.age,
                        user.groupNumber,
                        user.education.name,
                        user.englishLevel.name,
                        user.account.role.name
                    )
                }
            )
        } ?: KursApi.Error("No requests")

    fun updateUsers(accs: KursApi.UserList) {
        accs.users.forEach { user ->
            accountsDao.findByUsername(user.username)?.let {
                accountsDao.updateAccount(it.copy(role = UserRoles.valueOf(user.role)))
                userDao.save(
                    UserAo(
                        id = user.id,
                        account = it,
                        surname = user.surname,
                        name = user.name,
                        patronymic = user.patronymic,
                        age = user.age,
                        groupNumber = user.groupNumber,
                        education = Education.valueOf(user.education),
                        englishLevel = EnglishLevel.valueOf(user.englishLevel)
                    )
                )
            }
        }
    }

    fun deleteUser(id: Int) {
        userDao.deleteUser(id)
    }

    fun getAllUsers() = userDao.getAllUsers()
}