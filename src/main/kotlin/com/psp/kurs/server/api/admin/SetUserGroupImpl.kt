package com.psp.kurs.server.api.admin

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService

class SetUserGroupImpl(username: String, group: Int) : KursApi.SetUserGroup(username, group) {

    private val userService: UserService = UserService()

    override fun execute(): KursApi.Object = userService.addUserToGroup(username, groupId)
}