package com.psp.kurs.server.api.login

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService

class GetUserByTokenImpl(token: String) : KursApi.GetUserByToken(token) {

    private val userService: UserService = UserService()

    override fun execute(): KursApi.Object = userService.getUserByToken(token)
}