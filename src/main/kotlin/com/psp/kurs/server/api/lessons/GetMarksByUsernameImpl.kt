package com.psp.kurs.server.api.lessons

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.MarkService

class GetMarksByUsernameImpl(username: String) : KursApi.GetMarksByUsername(username) {

    private val markService: MarkService = MarkService()

    override fun execute(): KursApi.Object = markService.getMarksForUser(username).takeIf { it.isNotEmpty() }
            ?.run {
                KursApi.MarkList(map { KursApi.Mark(it.id, username, it.value, it.date) })
            }
            ?: KursApi.Error("No marks for student")
}