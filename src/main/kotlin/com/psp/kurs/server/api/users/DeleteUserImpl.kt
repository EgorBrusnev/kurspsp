package com.psp.kurs.server.api.users

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService

class DeleteUserImpl(id: Int) : KursApi.DeleteUser(id) {

    private val userService = UserService()

    override fun execute(): KursApi.Object =
        try {
            userService.deleteUser(id)
            KursApi.Ok()
        } catch (e: Exception) {
            KursApi.Error(e.localizedMessage)
        }
}