package com.psp.kurs.server.api.users

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService

class GetAllUsersImpl : KursApi.GetAllUsers() {
    private val userService = UserService()

    override fun execute(): KursApi.Object = userService.getAllUsers().takeIf { it.isNotEmpty() }?.let {
        KursApi.UserList(it.map {
            KursApi.User(
                it.id,
                it.account.username,
                it.surname,
                it.name,
                it.patronymic,
                it.age,
                it.groupNumber,
                it.education.name,
                it.englishLevel.name,
                it.account.role.name
            )
        })
    } ?: KursApi.Error("No users")
}