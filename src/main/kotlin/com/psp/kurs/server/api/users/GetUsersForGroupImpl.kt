package com.psp.kurs.server.api.users

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService

class GetUsersForGroupImpl(group: Int) : KursApi.GetUsersForGroup(group) {
    private val userService: UserService = UserService()

    override fun execute(): KursApi.Object =
        userService.findUsersForGroup(groupId).takeIf { it.isNotEmpty() }?.let {
            KursApi.UserList(
                it.map {
                    KursApi.User(
                        it.id,
                        it.account.username,
                        it.surname,
                        it.name,
                        it.patronymic,
                        it.age,
                        it.groupNumber,
                        it.education.name,
                        it.englishLevel.name,
                        it.account.role.name
                    )
                }
            )
        } ?: KursApi.Error("No users form group")

}