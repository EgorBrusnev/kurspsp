package com.psp.kurs.server.api.users

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService
import java.lang.Exception

class UpdateUsersImpl(users: KursApi.UserList) : KursApi.UpdateUsers(users) {

    private val userService = UserService()

    override fun execute(): KursApi.Object = try {
        userService.updateUsers(users)
        KursApi.Ok()
    } catch (e: Exception) {
        KursApi.Error(e.localizedMessage)
    }

}