package com.psp.kurs.server.api.lessons

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.MarkService

class AddMarkImpl(mark: KursApi.Mark) : KursApi.AddMark(mark) {

    private val markService = MarkService()

    override fun execute(): KursApi.Object = markService.addMark(mark)?.let {
        KursApi.Ok()
    } ?: KursApi.Error("Cannot add mark")
}