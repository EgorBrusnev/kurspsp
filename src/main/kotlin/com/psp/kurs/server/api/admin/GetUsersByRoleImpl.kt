package com.psp.kurs.server.api.admin

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService
import com.psp.kurs.server.utils.UserRoles

class GetUsersByRoleImpl(role: String) : KursApi.GetUsersByRole(role) {

    private val userService = UserService()

    override fun execute(): KursApi.Object = try{
        userService.findUsersByRole(UserRoles.valueOf(role))
    } catch (e: Exception) {
        e.printStackTrace()
        KursApi.Error("Error on finding role $role")
    }
}