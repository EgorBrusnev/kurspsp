package com.psp.kurs.server.api.login

import com.psp.kurs.server.dto.AccountDto
import com.psp.kurs.server.dto.UserDto
import com.psp.kurs.server.entity.UserAo
import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.LoginService
import com.psp.kurs.server.utils.Education
import com.psp.kurs.server.utils.EnglishLevel
import com.psp.kurs.server.utils.RegistrationException
import com.psp.kurs.server.utils.UserRoles

class RegisterUserImpl(account: KursApi.Account, user: KursApi.User) : KursApi.RegisterUser(account, user) {

    override fun execute(): KursApi.Object = try {
        LoginService().registerUser(
            AccountDto(account.username, account.password, role = UserRoles.valueOf(account.role)),
            UserDto(
                account.username,
                user.surname,
                user.name,
                user.patronymic,
                user.age,
                Education.valueOf(user.education),
                EnglishLevel.valueOf(user.englishLevel)
            )
        ).run { KursApi.AuthenticationResult(account.token, true, account.role.name) }
    } catch (e: RegistrationException) {
        KursApi.Error(e.message)
    }
}
