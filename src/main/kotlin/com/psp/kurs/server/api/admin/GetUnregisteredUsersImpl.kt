package com.psp.kurs.server.api.admin

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.UserService

class GetUnregisteredUsersImpl : KursApi.GetUnRegisteredUsers() {

    private val userService = UserService()

    override fun execute(): KursApi.Object = userService.getUnregisteredUsers()
}