package com.psp.kurs.server.api.lessons

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.ScheduleService

class GetScheduleByGroupImpl(groupId: Int) : KursApi.GetScheduleByGroup(groupId) {

    private val scheduleService = ScheduleService()

    override fun execute(): KursApi.Object = scheduleService.getScheduleByGroup(groupId)
        .takeIf { it.isNotEmpty() }?.run {
            KursApi.ScheduleList(
                map {
                    KursApi.Schedule(
                        it.id,
                        it.lesson,
                        it.groupId,
                        it.teacher,
                        it.room,
                        it.startAt.toLocalDateTime(),
                        it.endAt.toLocalDateTime()
                    )
                }
            )
        } ?: KursApi.Error("No lessons for this group")

}