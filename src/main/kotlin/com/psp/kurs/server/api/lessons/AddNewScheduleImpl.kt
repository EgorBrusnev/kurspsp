package com.psp.kurs.server.api.lessons

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.ScheduleService

class AddNewScheduleImpl(schedule: KursApi.Schedule) : KursApi.AddNewSchedule(schedule) {
    private val service: ScheduleService = ScheduleService()

    override fun execute(): KursApi.Object =
            service.addNewSchedule(schedule)
                    ?.let { KursApi.Ok() }
                    ?: KursApi.Error("Unable to add schedule")
}