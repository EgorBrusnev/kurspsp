package com.psp.kurs.server.api.lessons

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.MarkService

class GetMarksForGroupImpl(group: Int) : KursApi.GetMarksForGroup(group) {

    private val markService: MarkService = MarkService()

    override fun execute(): KursApi.Object =
            markService.getMarksForGroup(groupId).takeIf { it.isNotEmpty() }?.run {
                KursApi.MarkList(map { KursApi.Mark(it.id, it.user.account.username, it.value, it.date) })
            } ?: KursApi.Error("No marks for group")
}