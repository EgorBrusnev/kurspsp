package com.psp.kurs.server.api.login

import com.psp.kurs.server.dao.AccountsDao
import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.LoginService

class SendLoginTokenImpl(token: String?) : KursApi.SendLoginToken(token) {
    override fun execute(): KursApi.Object = LoginService().authenticateUser(token)
        ?.let { KursApi.AuthenticationResult(it.token, true, it.role?.name) }
            ?: KursApi.AuthenticationResult(isSuccess = false)
}