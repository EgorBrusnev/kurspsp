package com.psp.kurs.server.api.groups

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.GroupService

class GetAllGroupsImpl : KursApi.GetAllGroups() {
    private val groupService: GroupService = GroupService()

    override fun execute(): KursApi.Object = groupService.getAllGroups()
}