package com.psp.kurs.server.api.login

import com.psp.kurs.server.model.KursApi
import com.psp.kurs.server.service.LoginService

class SendLoginRequestImpl(username: String?, password: String?) : KursApi.SendLoginRequest(username, password) {
    override fun execute(): KursApi.Object =
        LoginService().authenticateUser(username, password)?.run {
            KursApi.AuthenticationResult(token, true, role.name)
        } ?: KursApi.AuthenticationResult(isSuccess = false)
}